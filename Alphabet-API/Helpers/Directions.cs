﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alphabet_API.Helpers
{
    public static class Directions
    {
        public static string Horizontal = "Horizontal";
        public static string InvertedHorizontal = "Horizontal Invertida";
        public static string Vertical = "Vertical";
        public static string InvertedVertical = "Vertical Invertida";
        public static string Diagonal = "Diagonal";
        public static string InvertedDiagonal = "Diagonal Invertida";
        public static string Special = "Escalera";
        public static string InvertedSpecial = "Escalera Invertida";
    }
}