﻿using Newtonsoft.Json;

namespace Soup_NetCore.Models
{
    public class ApiValidationResult
    {
        public ApiValidationResult() { }

        public ApiValidationResult(string word)
        {
            Word = word;
            WordExists = false;
            Direction = "";
        }

        public ApiValidationResult(string word, bool wordExists, string direction)
        {
            Word = word;
            WordExists = wordExists;
            Direction = direction;
        }

        [JsonProperty("Word")]
        public string Word { get; set; }

        [JsonProperty("WordExists")]
        public bool WordExists { get; set; }

        [JsonProperty("Direction")]
        public string Direction { get; set; }


    }
}