﻿namespace Soup_NetCore.Models
{
    public class LetterLocation
    {
        public LetterLocation() { }

        public LetterLocation(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public int Row { get; set; }
        public int Column { get; set; }
    }
}
