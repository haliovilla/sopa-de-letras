﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Soup_NetCore.Models
{
    public class AlphabetModel
    {
        [Required]
        [JsonProperty("AlphabetSoup")]
        public List<string> AlphabetSoup { get; set; }

        [Required]
        [JsonProperty("WordToFind")]
        public string WordToFind { get; set; }

        [Required]
        [JsonProperty("SoupSize")]
        public int SoupSize { get; set; }
    }
}
