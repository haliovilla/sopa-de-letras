export interface ApiResult {
  word: string;
  wordExists: boolean;
  direction: string;
}
